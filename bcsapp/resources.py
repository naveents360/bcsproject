from .models import *

from import_export import resources

class CustomerResource(resources.ModelResource):
    class Meta:
        model = Customer
