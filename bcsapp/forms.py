from django import forms
from .models import *

class CustomerForm(forms.ModelForm):
    class Meta:
        model = Customer
        fields = '__all__'

class PolicyForm(forms.ModelForm):
    class Meta:
        model = Policy
        fields = '__all__'    
