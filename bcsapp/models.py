from django.db import models
from datetime import date
# Create your models here.
GENDER_CHOICES = (
        ("Male","MALE"),
        ("Female","FEMALE"),
)

REGION_CHOICES = (
        ("North","NORTH"),
        ("South","SOUTH"),
        ("East","EAST"),
        ("West","WEST"),
) 

INCOME_GROUP = (
        ("0-$25k","0-$25k"),
        ("$25k-$70k","$25k-$70k"),
        (">$70K",">$70K"),
)

MARITAL_STATUS_CHOICES = (
        ("0","UNMARRIED"),
        ("1","MARRIED"),
)

FUEL_CHOICES =(
        ("CNG","CNG"),
        ("Petrol","PETROL"),
        ("Diesel","DIESEL"),
)

VEHICLE_SEGMENT_CHOICES= (
        ("A","A"),
        ("B","B"),
        ("C","C"),
)

EXTRA_CHOICES = (
        ("0","0"),
        ("1","1"),
)

class Customer(models.Model):
    id=models.IntegerField(primary_key=True)
    Gender = models.CharField(max_length=6,choices=GENDER_CHOICES)
    Income_group = models.CharField(max_length=9,choices=INCOME_GROUP)
    Region = models.CharField(max_length=5,choices=REGION_CHOICES)
    Marital_status = models.CharField(max_length=1,choices=MARITAL_STATUS_CHOICES)

class Policy(models.Model):
    id=models.IntegerField(primary_key=True)
    Date_of_Purchase = models.DateField(blank=True,null=True,auto_now=True)
    customer = models.ForeignKey(Customer,on_delete=models.CASCADE)
    Fuel = models.CharField(max_length=7,choices=FUEL_CHOICES,default="CNG")
    Vehicle_segment=models.CharField(max_length=1,choices=VEHICLE_SEGMENT_CHOICES,default="A")
    Premium = models.IntegerField(null = True)
    Bodily_injury_liability = models.CharField(max_length=1,choices=EXTRA_CHOICES,default="0")
    Personal = models.CharField(max_length=1,choices=EXTRA_CHOICES,default="0")
    Collision = models.CharField(max_length=1,choices=EXTRA_CHOICES,default="0")
    Property = models.CharField(max_length=1,choices=EXTRA_CHOICES,default="0")
    Comprehensive = models.CharField(max_length=1,choices=EXTRA_CHOICES,default="0")
