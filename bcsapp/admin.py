from django.contrib import admin

# Register your models here.
from .models import *
#from import_export.admin import ImportExportModelAdmin
from import_export.admin import ImportExportModelAdmin

#admin.site.register(Policy)
#admin.site.register(Customer)

@admin.register(Customer)
class CustomerAdmin(ImportExportModelAdmin):
    pass
