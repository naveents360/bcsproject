from django.shortcuts import render
from django.db.models import Count
from django.db.models.functions import TruncMonth
# Create your views here.
from .models import *
from .serializer import *
from .forms import *
from rest_framework.decorators import api_view
from django.db.models import Q
from django.http import JsonResponse
import json
import pandas
import datetime as dt

@api_view(['GET'])
def getPolicyDetailsById(request):
    id = request.query_params.get('policy_id')
    policy = Policy.objects.get(id=id)
    policyserializer = PolicySerializer(policy)
    if(len(policyserializer.data) == 0):
        return JsonResponse({'msg':'No Records Found'},status=404,safe=False)
    return JsonResponse(policyserializer.data,safe=False)

@api_view(['GET'])
def getPolicyDetailsByIdFilter(request):
    id = request.query_params.get('policy_id')
    policy = Policy.objects.filter(id=id)
    policyserializer = PolicySerializer(policy,many=True)
    if(len(policyserializer.data) == 0):
        return JsonResponse({'msg':'No Records Found'},status=404,safe=False)
    return JsonResponse(policyserializer.data,safe=False)

@api_view(['GET'])
def getPolicyDetailsByCustomerId(request):
    cust_id=request.query_params.get('cust_id')
    policy = Policy.objects.filter(customer=cust_id)
    policyserializer = PolicySerializer(policy,many=True)
    if(len(policyserializer.data) == 0):
        return JsonResponse({'msg':'No Records Found'},status=404,safe=False)
    return JsonResponse(policyserializer.data,safe=False)

@api_view(['POST'])
def updatePremiumByPolicyId(request):
    id = request.query_params.get('policy_id')
    policy = Policy.objects.get(id=id)
    policydetails = json.loads(request.body)
    print(policy.customer.id)
    policy.Premium = policydetails.get('Premium')
    policy.Fuel=policydetails.get('Fuel')
    policy.Vehicle_segment =policydetails.get('Vehicle_segment')
    policy.Bodily_injury_liability = policydetails.get('Bodily_injury_liability')
    policy.Personal = policydetails.get('Personal')
    policy.Collision = policydetails.get('Collision')
    policy.Property = policydetails.get('Property')
    policy.Comprehensive = policydetails.get('Comprehensive')
    policy.customer.id = policydetails.get('customer')
    policy.save()
    return JsonResponse({'msg':'Details Updated Successfully'},status=200,safe=False)

@api_view(['GET'])
def getAllPolicyDetails(request):
    policy = Policy.objects.all()
    policyserializer = PolicySerializer(policy,many=True)
    return JsonResponse(policyserializer.data,safe=False)

@api_view(['POST'])
def createCustomer(request):
    customer = json.loads(request.body)
    count = Customer.objects.all().count()
    customer.update({'id':count+1})
    customerform = CustomerForm(customer)
    if customerform.is_valid():
        customerform.save(commit=True)
        return JsonResponse({'msg':'Customer Created Successfully'},status=200,safe=False)
    else:
        print(customerform.errors)
        return JsonResponse({'msg':'Invalid Data'},status=400,safe=False)

@api_view(['POST'])
def createPolicy(request):
    policy = json.loads(request.body)
    print(type(policy))
    count = Policy.objects.all().count()
    policy.update({'id':count+1})
    policyform = PolicyForm(policy)
    if policyform.is_valid():
        policyform.save(commit=True)
        return JsonResponse({'msg':'Policy Created Successfully'},safe=False)
    else:
        return Jsonresponse({'msg':'Invalid Data'},status=400,safe=False)

@api_view(['GET'])
def getPolicyDetailsByMonth(request):
    #month = request.query_params.get('month')
    year = int(request.query_params.get('year'))
    #region = request.query_params.get('region')
    #policies = Policy.objects.filter(Date_of_Purchase__year = year,Date_of_Purchase__month=month,customer__Region=region)
    fs = Policy.objects.filter(Date_of_Purchase__year = year).annotate(month=TruncMonth('Date_of_Purchase')).values('month').annotate(c=Count('id')).values('month','c')
    arr = [0,0,0,0,0,0,0,0,0,0,0,0]
    for each in fs:
        arr[(int(each['month'].month)-1)]=each['c']
    print(arr)
    #policyserializer = PolicySerializer(policies,many=True)
    return JsonResponse({'msg':arr},safe=False)

@api_view(['GET'])
def getPolicyDetailsByRegion(request):
    region = request.query_params.get('region')
    pass

@api_view(['GET'])
def InsertDemoDataApi(request):
    filepath = "/root/coding/bcsproject/SampleData.xlsx"
    sampledata = pandas.read_excel(filepath,sheet_name='SampleData')
    json_sampledata = sampledata.to_json(orient='records')
    data = json.loads(json_sampledata)
    customerdict = {}
    policydict = {}
    for es in data:
        datestr = str(es['Date of Purchase'])
        datestr = datestr.strip()
        datestr = datestr.replace('-',',')
        datestr = datestr.replace('/',',')
        customerdict.update({'id':es['Customer_id'],'Gender':es['Customer_Gender'],'Income_group':es['Customer_Income group'].replace(' ',''),'Region':es['Customer_Region'],"Marital_status":es['Customer_Marital_status']})
        policydict.update({'id':es['Policy_id'],'customer':es['Customer_id'],'Fuel':es['Fuel'],'Vehicle_segment':es['VEHICLE_SEGMENT'],'Premium':es['Premium'],'Bodily_injury_liability':es['bodily injury liability'],'Personal':es[' personal injury protection'],'Collision':es[' collision'],'Property':es[' property damage liability'],'Comprehensive':es[' comprehensive'],'Date of Purchase':dt.datetime.strptime(datestr,'%m,%d,%Y')})
        custmerform = CustomerForm(customerdict)
        print(customerdict)
        policyform = PolicyForm(policydict)
        print(custmerform.errors)
        custmerform.save()
        policyform.save()
        #print(es['Policy_id'])
    #print(json_sampledata)
    #print(sampledata)
    return JsonResponse({'msg':'success'},safe=False)
