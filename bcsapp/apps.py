from django.apps import AppConfig


class BcsappConfig(AppConfig):
    name = 'bcsapp'
