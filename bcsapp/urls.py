from .views import *
from django.urls import path

urlpatterns = [
        path('getPolicyDetailsById', getPolicyDetailsById),
        path('getPolicyDetailsByCustomerId',getPolicyDetailsByCustomerId),
        path('updatePremiumByPolicyId',updatePremiumByPolicyId),
        path('getAllPolicyDetails',getAllPolicyDetails),
        path('createCustomer',createCustomer),
        path('createPolicy',createPolicy),
        path('InsertDemoDataApi',InsertDemoDataApi),
        path('getPolicyDetailsByIdFilter',getPolicyDetailsByIdFilter),
        path('getPolicyDetailsByMonth',getPolicyDetailsByMonth),
        ]
